use mlua::{Error, Lua};

static MODULE_SCRIPT: &str = include_str!("ltn12.lua");

pub fn preload(lua: &Lua) -> Result<(), Error> {
    let script = format!("package.preload['ltn12'] = function() {} end", MODULE_SCRIPT);
    lua.load(script).exec()?;
    Ok(())
}

#[cfg(test)]
mod tests {
    use mlua::{Lua, Table};
    use std::error::Error;

    #[test]
    fn preload() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::preload(&lua)?;
        let _module: Table = lua
            .load(
                r#"
                return require('ltn12')
            "#,
            )
            .eval()?;
        Ok(())
    }
}
