use base64::{engine::general_purpose, Engine as _};
use mlua::{Error, Lua};

pub(super) fn handle(_lua: &Lua, data: mlua::String) -> Result<String, Error> {
    let encoded: String = general_purpose::STANDARD.encode(data.as_bytes());
    Ok(encoded)
}

#[cfg(test)]
mod tests {
    use mlua::Lua;
    use std::error::Error;

    #[test]
    fn b64() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::preload(&lua)?;
        let encoded: String = lua
            .load(
                r#"
                local mime = require('mime')
                return mime.b64('abcd')
            "#,
            )
            .eval()?;
        assert_eq!(encoded, "YWJjZA==");
        Ok(())
    }
}
