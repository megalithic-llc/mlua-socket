use mlua::{Error, Lua};

static MODULE_SCRIPT: &str = include_str!("url.lua");

pub fn preload(lua: &Lua) -> Result<(), Error> {
    let script = format!("package.preload['socket.url'] = function() {} end", MODULE_SCRIPT);
    lua.load(script).exec()?;
    Ok(())
}

#[cfg(test)]
mod tests {
    use mlua::Lua;
    use std::error::Error;

    #[test]
    fn preload() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::preload(&lua)?;
        let (scheme, path): (String, String) = lua
            .load(
                r#"
                local url = require('socket.url')
                local parsed_url = url.parse('http://www.example.com/cgilua/index.lua')
                return parsed_url.scheme, parsed_url.path 
            "#,
            )
            .eval()?;
        assert_eq!(scheme, "http");
        assert_eq!(path, "/cgilua/index.lua");
        Ok(())
    }
}
