use super::tcp::Tcp;
use mlua::{Error, Lua, MultiValue};
use std::os::fd::AsRawFd;

pub(super) fn handle(_lua: &Lua, tcp: &Tcp, _args: MultiValue) -> Result<i32, Error> {
    let fd = tcp.socket.as_raw_fd();
    Ok(fd)
}

#[cfg(test)]
mod tests {
    use mlua::Lua;
    use std::error::Error;

    #[tokio::test]
    async fn getfd_server() -> Result<(), Box<dyn Error>> {
        // Setup a listener
        let lua = Lua::new();
        crate::preload(&lua)?;
        let script = r#"
                local socket = require('socket')
                local server = assert(socket.bind('127.0.0.1', 0))
                return server:getfd()
            "#;
        let fd: i32 = lua.load(script).eval()?;
        assert!(fd > 0);
        Ok(())
    }
}
