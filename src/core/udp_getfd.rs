use super::udp::Udp;
use mlua::{Error, Lua, MultiValue};
use std::os::fd::AsRawFd;

pub(super) fn handle(_lua: &Lua, udp: &Udp, _args: MultiValue) -> Result<i32, Error> {
    let socket = udp.socket.lock().map_err(|err| Error::RuntimeError(err.to_string()))?;
    let fd = socket.as_raw_fd();
    Ok(fd)
}
