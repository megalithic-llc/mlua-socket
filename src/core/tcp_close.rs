use super::tcp::Tcp;
use mlua::{Error, Lua};
use std::net::Shutdown;

pub(super) fn handle(_lua: &Lua, tcp: &Tcp) -> Result<(), Error> {
    let _ = tcp.socket.shutdown(Shutdown::Both);
    Ok(())
}

#[cfg(test)]
mod tests {
    extern crate tokio;

    use mlua::Lua;
    use std::error::Error;
    use std::net::SocketAddr;
    use tokio::io::AsyncReadExt;
    use tokio::net::TcpListener;

    #[tokio::test]
    async fn close() -> Result<(), Box<dyn Error>> {
        // Setup a listener to connect to and a receiver
        let addr = "127.0.0.1:0".parse::<SocketAddr>()?;
        let listener = TcpListener::bind(&addr).await?;
        let port = listener.local_addr()?.port();
        tokio::spawn(async move {
            let (mut socket, _) = listener.accept().await.unwrap();
            let mut buf = vec![0; 3];
            let _ = socket.read_exact(&mut buf).await;
        });

        // Test
        let lua = Lua::new();
        crate::preload(&lua)?;
        let script = r#"
                local socket = require('socket')
                local master = socket.tcp()
                local ok, err = master:connect('127.0.0.1', _port_)
                assert(ok)
                master:close()
                return master:send('abc')
            "#
        .replace("_port_", format!("{}", port).as_str());
        let (_bytes_sent, err): (Option<u16>, Option<String>) = lua.load(script).eval()?;
        assert_ne!(err, None);
        Ok(())
    }
}
