mod core;
mod dns;
mod except;
mod headers;
mod http;
mod ltn12;
mod mime;
mod url;

use mlua::{Error, Lua};

static MODULE_SCRIPT: &str = include_str!("socket.lua");

pub fn preload(lua: &Lua) -> Result<(), Error> {
    // Preload modules
    dns::preload(lua)?;
    headers::preload(lua)?;
    ltn12::preload(lua)?;
    mime::preload(lua)?;
    url::preload(lua)?;
    except::preload(lua)?;
    core::preload(lua)?;
    http::preload(lua)?;

    let script = format!("package.preload['socket'] = function() {} end", MODULE_SCRIPT);
    lua.load(script).exec()?;

    Ok(())
}

#[cfg(test)]
mod tests {
    use mlua::{Lua, Table};
    use std::error::Error;

    #[test]
    fn load() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::preload(&lua)?;
        let _module: Table = lua.load("return require('socket')").eval()?;
        // assert!(module.contains_key("core")?);
        // assert!(module.contains_key("dns")?);
        // assert!(module.contains_key("http")?);
        // assert!(module.contains_key("headers")?);
        // assert!(module.contains_key("url")?);
        Ok(())
    }
}
