use mlua::{Error, Lua};

static MODULE_SCRIPT: &str = include_str!("except.lua");

pub fn preload(lua: &Lua) -> Result<(), Error> {
    let script = format!("package.preload['socket.except'] = function() {} end", MODULE_SCRIPT);
    lua.load(script).exec()?;
    Ok(())
}

#[cfg(test)]
mod tests {
    use mlua::{Lua, Table};
    use std::error::Error;

    #[test]
    fn preload() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::preload(&lua)?;
        let _module: Table = lua
            .load(
                r#"
                return require('socket.except')
            "#,
            )
            .eval()?;
        Ok(())
    }

    #[test]
    fn excepttest() -> Result<(), Box<dyn Error>> {
        static EXCEPTTEST_SCRIPT: &str = include_str!("excepttest.lua");
        let lua = Lua::new();
        crate::preload(&lua)?;
        lua.load(EXCEPTTEST_SCRIPT).exec()?;
        Ok(())
    }
}
