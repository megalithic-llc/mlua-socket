mod request;

use mlua::{Error, Lua, Table};

#[allow(unused)]
static MODULE_SCRIPT: &str = include_str!("http.lua");

pub fn preload(lua: &Lua) -> Result<(), Error> {
    // Configure module table
    let table = lua.create_table()?;
    table.set("request", lua.create_function(request::handle)?)?;

    // Preload module
    let globals = lua.globals();
    let package: Table = globals.get("package")?;
    let loaded: Table = package.get("loaded")?;
    loaded.set("socket.http", table)?;
    Ok(())
}

#[cfg(test)]
mod tests {
    use mlua::{Lua, Table};
    use std::error::Error;

    #[test]
    fn preload() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::preload(&lua)?;
        let _module: Table = lua
            .load(
                r#"
                return require('socket.http')
            "#,
            )
            .eval()?;
        Ok(())
    }
}
