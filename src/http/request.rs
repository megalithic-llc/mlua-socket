use mlua::{Error, FromLua, Lua, MultiValue};

pub(super) fn handle(lua: &Lua, args: MultiValue) -> Result<(Option<String>, u16), Error> {
    // Parse args
    if let Ok(url) = String::from_lua(args[0].clone(), lua) {
        handle_simple(lua, &url)
    } else {
        Err(Error::RuntimeError(
            "Non-string request argument is not yet supported".to_string(),
        ))
    }
}

fn handle_simple(_lua: &Lua, url: &str) -> Result<(Option<String>, u16), Error> {
    let res = reqwest::blocking::get(url).map_err(|e| Error::RuntimeError(e.to_string()))?;
    let status_code = res.status().as_u16();
    let body = match res.text() {
        Ok(body) => Ok(body),
        Err(e) => Err(Error::RuntimeError(e.to_string())),
    }?;
    Ok((Some(body), status_code))
}

#[cfg(test)]
mod tests {
    use mlua::Lua;
    use std::error::Error;

    #[test]
    #[ignore]
    fn test() -> Result<(), Box<dyn Error>> {
        let lua = Lua::new();
        crate::preload(&lua)?;
        let s: String = lua
            .load(
                r#"
                return require('socket.http').request('https://apt.on-prem.net/public.key')
            "#,
            )
            .eval()?;
        assert!(s.starts_with("-----BEGIN PGP PUBLIC KEY BLOCK-----\nVersion: GnuPG v1\n\n"));
        Ok(())
    }
}
